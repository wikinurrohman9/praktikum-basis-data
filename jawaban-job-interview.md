## JAWABAN UTS  PRAKTIKUM BASIS DATA

# Nomor 1
Perancangan database diagram traveloka berdasarkan permasalahan dunia nyata meliputi beberapa transaksi seperti pemesanan tiket pesawat dan booking hotel.
![Untitled (2)](https://github.com/wikinurrohman/Prak-Basis-Data/assets/95328207/ff5fcb9f-5034-43fa-aeb4-5ee5985cba51)

# Nomor 2
# DDL (Data Definition Language)
Pembuatan tabel berdasarkan diagram pada nomor 1 dengan mengekspor DBML pada dbdiagram menjadi DDL MySQL
```SQL
CREATE TABLE `Pengguna` (
  `id_pengguna` int PRIMARY KEY,
  `nama_pengguna` varchar(255),
  `email` varchar(255),
  `kata_sandi` varchar(255),
  `nama_depan` varchar(255),
  `nama_belakang` varchar(255),
  `nomor_telepon` varchar(255),
  `alamat` varchar(255),
  `jenis_kelamin` enum
);

CREATE TABLE `Hotel` (
  `id_hotel` int PRIMARY KEY,
  `nama_hotel` varchar(255),
  `alamat` varchar(255),
  `kota` varchar(255),
  `negara` varchar(255),
  `peringkat_bintang` decimal,
  `fasilitas` varchar(255),
  `deskripsi` varchar(255),
  `rata_rata_peringkat` decimal
);

CREATE TABLE `Penerbangan` (
  `id_penerbangan` int PRIMARY KEY,
  `maskapai_penerbangan` varchar(255),
  `bandara_keberangkatan` varchar(255),
  `bandara_kedatangan` varchar(255),
  `tanggal_keberangkatan` date,
  `tanggal_kedatangan` date,
  `waktu_keberangkatan` time,
  `waktu_kedatangan` time,
  `harga` decimal,
  `ketersediaan_kursi` int,
  `jenis_pesawat` varchar(255),
  `maskapai_penerbangan_fk` varchar(255)
);

CREATE TABLE `MaskapaiPenerbangan` (
  `id_maskapai` int PRIMARY KEY,
  `nama_maskapai` varchar(255),
  `kode_maskapai` varchar(255),
  `negara_asal` varchar(255)
);

CREATE TABLE `Pemesanan` (
  `id_pemesanan` int PRIMARY KEY,
  `id_pengguna` int,
  `tanggal_pemesanan` date,
  `total_harga` decimal,
  `status` varchar(255),
  `id_pengguna_fk` int
);

CREATE TABLE `PemesananHotel` (
  `id_pemesanan_hotel` int PRIMARY KEY,
  `id_pemesanan` int,
  `id_hotel` int,
  `tanggal_check_in` date,
  `tanggal_check_out` date,
  `jumlah_kamar` int,
  `jumlah_tamu` int,
  `id_pemesanan_fk` int,
  `id_hotel_fk` int
);

CREATE TABLE `PemesananPenerbangan` (
  `id_pemesanan_penerbangan` int PRIMARY KEY,
  `id_pemesanan` int,
  `id_penerbangan` int,
  `jumlah_penumpang` int,
  `id_pemesanan_fk` int,
  `id_penerbangan_fk` int
);

CREATE TABLE `Pembayaran` (
  `id_pembayaran` int PRIMARY KEY,
  `id_pemesanan` int,
  `tanggal_pembayaran` date,
  `jumlah` decimal,
  `metode_pembayaran` varchar(255),
  `status_pembayaran` varchar(255),
  `id_pemesanan_fk` int
);

CREATE TABLE `Review` (
  `id_review_hotel` int,
  `id_review_penerbangan` int,
  `id_pengguna` int,
  `id_hotel` int,
  `id_penerbangan` int,
  `tanggal_review` date,
  `rating` decimal,
  `komentar` varchar(255),
  `id_pengguna_fk` int,
  `id_hotel_fk` int,
  `id_penerbangan_fk` int,
  PRIMARY KEY (`id_review_hotel`, `id_review_penerbangan`)
);

ALTER TABLE `Penerbangan` ADD FOREIGN KEY (`maskapai_penerbangan_fk`) REFERENCES `MaskapaiPenerbangan` (`id_maskapai`);

ALTER TABLE `Pemesanan` ADD FOREIGN KEY (`id_pengguna_fk`) REFERENCES `Pengguna` (`id_pengguna`);

ALTER TABLE `PemesananHotel` ADD FOREIGN KEY (`id_pemesanan_fk`) REFERENCES `Pemesanan` (`id_pemesanan`);

ALTER TABLE `PemesananHotel` ADD FOREIGN KEY (`id_hotel_fk`) REFERENCES `Hotel` (`id_hotel`);

ALTER TABLE `PemesananPenerbangan` ADD FOREIGN KEY (`id_pemesanan_fk`) REFERENCES `Pemesanan` (`id_pemesanan`);

ALTER TABLE `PemesananPenerbangan` ADD FOREIGN KEY (`id_penerbangan_fk`) REFERENCES `Penerbangan` (`id_penerbangan`);

ALTER TABLE `Pembayaran` ADD FOREIGN KEY (`id_pemesanan_fk`) REFERENCES `Pemesanan` (`id_pemesanan`);

ALTER TABLE `Review` ADD FOREIGN KEY (`id_pengguna_fk`) REFERENCES `Pengguna` (`id_pengguna`);

ALTER TABLE `Review` ADD FOREIGN KEY (`id_hotel_fk`) REFERENCES `Hotel` (`id_hotel`);

ALTER TABLE `Review` ADD FOREIGN KEY (`id_penerbangan_fk`) REFERENCES `Penerbangan` (`id_penerbangan`);

```

# Nomor 3
# DML (Data Manipulation Language)
Serangkaian perintah untuk memanipulasi data yang ada di Database
| No. | Use Case                                   |
|-----|--------------------------------------------|
| 1   | Registrasi Pengguna                         |
| 2   | Masuk Pengguna                              |
| 3   | Pemulihan Kata Sandi                        |
| 4   | Profil Pengguna                             |
| 5   | Pembaruan Profil Pengguna                   |
| 6   | Hapus Akun Pengguna                         |
| 7   | Daftar Hotel                                |
| 8   | Pembaruan Informasi Hotel                   |
| 9   | Hapus Hotel                                 |
| 10  | Daftar Penerbangan                          |
| 11  | Pembaruan Informasi Penerbangan             |
| 12  | Hapus Penerbangan                           |
| 13  | Riwayat Pemesanan Pengguna                  |
| 14  | Pembatalan Pemesanan                        |
| 15  | Laporan Penjualan                           |
| 16  | Statistik Pengguna                          |
| 17  | Statistik Hotel                             |
| 18  | Statistik Penerbangan                       |
| 19  | Pemberian Diskon                            |
| 20  | Manajemen Fasilitas Hotel                    |
| 21  | Pencarian Pengguna Berdasarkan Kriteria      |
| 22  | Pencarian Hotel Berdasarkan Kriteria         |
| 23  | Pencarian Penerbangan Berdasarkan Kriteria   |
| 24  | Pemesanan Gabungan Hotel dan Penerbangan     |
| 25  | Pelacakan Status Pemesanan                  |
| 26  | Pencarian Pengguna                           |
| 27  | Pencarian Pembayaran                         |
| 28  | Pencarian Review                             |
| 29  | Analisis Popularitas Hotel                   |
| 30  | Analisis Rute Penerbangan Populer             |
| 31  | Manajemen Maskapai Penerbangan                |
| 32  | Pengelolaan Ketersediaan Fasilitas Hotel       |
| 33  | Pemesanan Hotel dengan Kupon Diskon           |
| 34  | Pengiriman Notifikasi Ketersediaan Kamar       |
| 35  | Analisis Pelanggan untuk Penawaran Khusus     |
| 36  | Riwayat Pembayaran Pengguna                   |
| 37  | Pencarian Pemesanan Hotel oleh Pengguna       |
| 38  | Pencarian Pemesanan Penerbangan oleh Pengguna |
| 39  | Pemesanan Kamar Hotel untuk Acara Khusus      |
| 40  | Pemesanan Penerbangan dengan Kelas Ekonomi    |
| 41  | Pemesanan Penerbangan dengan Kelas Bisnis     |
| 42  | Pemesanan Penerbangan dengan Kelas Pertama    |
| 43  | Pencatatan Log Aktivitas Pengguna             |
| 44  | Laporan Keuangan Harian                       |
| 45  | Pencatatan Riwayat Harga Penerbangan           |
| 46  | Pemesanan Kamar Hotel dengan Tipe Kamar        |
| 47  | Pemesanan Penerbangan dengan Tipe Pesawat      |
| 48  | Pemesanan Kamar Hotel dengan Layanan Tambahan  |
| 49  | Pemesanan Penerbangan dengan Layanan Tambahan  |
| 50  | Manajemen Hak Akses Pengguna                  |
| 51  | Pencarian Pengguna                            |
| 52  | Pencarian Pembayaran                          |
| 53  | Pencarian Review                              |
| 54  | Analisis Popularitas Hotel                    |
| 55  | Analisis Rute Penerbangan Populer              |
| 56  | Manajemen Maskapai Penerbangan                 |
| 57  | Pengelolaan Ketersediaan Fasilitas Hotel        |
| 58  | Pemesanan Hotel dengan Kupon Diskon            |
| 59  | Pengiriman Notifikasi Ketersediaan Kamar        |
| 60  | Analisis Pelanggan untuk Penawaran Khusus      |
| 61  | Riwayat Pembayaran Pengguna                    |
| 62  | Pencarian Pemesanan Hotel oleh Pengguna        |
| 63  | Pencarian Pemesanan Penerbangan oleh Pengguna  |
| 64  | Pemesanan Kamar Hotel untuk Acara Khusus       |
| 65  | Pemesanan Penerbangan dengan Kelas Ekonomi     |
| 66  | Pemesanan Penerbangan dengan Kelas Bisnis      |
| 67  | Pemesanan Penerbangan dengan Kelas Pertama     |
| 68  | Pencatatan Log Aktivitas Pengguna              |
| 69  | Laporan Keuangan Harian                        |
| 70  | Pencatatan Riwayat Harga Penerbangan            |
| 71  | Pemesanan Kamar Hotel dengan Tipe Kamar         |
| 72  | Pemesanan Penerbangan dengan Tipe Pesawat       |
| 73  | Pemesanan Kamar Hotel dengan Layanan Tambahan   |
| 74  | Pemesanan Penerbangan dengan Layanan Tambahan   |
| 75  | Manajemen Hak Akses Pengguna                   |

# -  Insert pada Tabel Pengguna
Berikut ini adalah syntax SQL untuk menginput data pada database tabel Pengguna
```mySQL
INSERT INTO `Pengguna` (`id_pengguna`, `nama_pengguna`, `email`, `kata_sandi`, `nama_depan`, `nama_belakang`, `nomor_telepon`, `alamat`, `jenis_kelamin`)
VALUES 
 (1, 'Wiki Nurrohman', 'wikinr@example.com', '1217050140', 'Wiki', 'Nurrohman', '082145567890', 'Subang', 'Laki-laki'),
  (2, 'Yudha Ristian Asgari', 'yudharasgar@example.com', '1217050145', 'Yudha', 'Ristian Asgari', '023487962364', 'Bandung Barat', 'Laki-laki'),
  (3, 'BudiHermawan', 'budihermawan@example.com', 'password789', 'Budi', 'Hermawan', '081234567892', 'Surabaya', 'Laki-laki'),
  (4, 'RinaSari', 'rinasari@example.com', 'passwordabc', 'Rina', 'Sari', '081234567893', 'Yogyakarta', 'Perempuan'),
  (5, 'AhmadSantoso', 'ahmadsantoso@example.com', 'passworddef', 'Ahmad', 'Santoso', '081234567894', 'Medan', 'Laki-laki'),
  (6, 'LindaWijaya', 'lindawijaya@example.com', 'passwordghi', 'Linda', 'Wijaya', '081234567895', 'Denpasar', 'Perempuan'),
  (7, 'HariantoPutra', 'hariantoputra@example.com', 'passwordjkl', 'Harianto', 'Putra', '081234567896', 'Makassar', 'Laki-laki'),
  (8, 'DewiLestari', 'dewilestari@example.com', 'passwordmno', 'Dewi', 'Lestari', '081234567897', 'Semarang', 'Perempuan'),
  (9, 'RudiKusuma', 'rudikusuma@example.com', 'passwordpqr', 'Rudi', 'Kusuma', '081234567898', 'Palembang', 'Laki-laki'),
  (10, 'LinaWahyuni', 'linawahyuni@example.com', 'passwordstu', 'Lina', 'Wahyuni', '081234567899', 'Padang', 'Perempuan'),
  (11, 'AriefSaputra', 'ariefsaputra@example.com', 'passwordvwx', 'Arief', 'Saputra', '081234567890', 'Bogor', 'Laki-laki'),
  (12, 'RitaKusnadi', 'ritakusnadi@example.com', 'passwordyz1', 'Rita', 'Kusnadi', '081234567891', 'Bandar Lampung', 'Perempuan'),
  (13, 'HendroWibowo', 'hendrowibowo@example.com', 'password234', 'Hendro', 'Wibowo', '081234567892', 'Tangerang', 'Laki-laki'),
  (14, 'AnitaKurniawan', 'anitakurniawan@example.com', 'password567', 'Anita', 'Kurniawan', '081234567893', 'Malang', 'Perempuan'),
  (15, 'FerrySusilo', 'ferrysusilo@example.com', 'password890', 'Ferry', 'Susilo', '081234567894', 'Surakarta', 'Laki-laki'),
  (16, 'NoviIndriani', 'noviindriani@example.com', 'password123', 'Novi', 'Indriani', '081234567895', 'Depok', 'Perempuan'),
  (17, 'HadiSantoso', 'hadisantoso@example.com', 'password456', 'Hadi', 'Santoso', '081234567896', 'Manado', 'Laki-laki'),
  (18, 'RinaAgustina', 'rinaagustina@example.com', 'password789', 'Rina', 'Agustina', '081234567897', 'Cirebon', 'Perempuan'),
  (19, 'HendraKusuma', 'hendrakusuma@example.com', 'passwordabc', 'Hendra', 'Kusuma', '081234567898', 'Bekasi', 'Laki-laki'),
  (20, 'DianSuryadi', 'diansuryadi@example.com', 'passworddef', 'Dian', 'Suryadi', '081234567899', 'Banda Aceh', 'Perempuan');
  ```

# - Insert pada Tabel Hotel
Berikut ini adalah syntax SQL untuk menginput data pada database tabel Hotel
```mySQL
INSERT INTO `Hotel` (`id_hotel`, `nama_hotel`, `alamat`, `kota`, `negara`, `peringkat_bintang`, `fasilitas`, `deskripsi`, `rata_rata_peringkat`)
VALUES 
  (1, 'Grand Hyatt Jakarta', 'Jalan M.H. Thamrin Kav. 28-30', 'Jakarta', 'Indonesia', '05.0', 'Kolam Renang, Spa, Restoran', 'Hotel mewah di pusat kota Jakarta', '8.9'),
  (2, 'The Trans Luxury Hotel', 'Jalan Gatot Subroto 289', 'Bandung', 'Indonesia', '05.0', 'Kolam Renang, Gym, Spa', 'Hotel mewah di pusat kota Bandung', '9.1'),
  (3, 'Ayana Resort and Spa', 'Jalan Karang Mas Sejahtera', 'Bali', 'Indonesia', '05.0', 'Kolam Renang, Pantai Pribadi, Restoran', 'Resor mewah dengan pemandangan laut di Bali', '9.3'),
  (4, 'The Phoenix Hotel Yogyakarta', 'Jalan Jenderal Sudirman 9', 'Yogyakarta', 'Indonesia', '04.0', 'Kolam Renang, Ruang Pertemuan', 'Hotel bersejarah dengan gaya kolonial di Yogyakarta', '8.4'),
  (5, 'Hotel Santika Premiere', 'Jalan Asia Afrika 123', 'Surabaya', 'Indonesia', '04.5', 'Kolam Renang, Restoran, Spa', 'Hotel modern di pusat kota Surabaya', '8.6');
```

# - Insert pada Tabel Penerbangan
Berikut ini adalah syntax SQL untuk menginput data pada database tabel Penerbangan
```mySQL
INSERT INTO `Penerbangan` (`id_penerbangan`, `maskapai_penerbangan`, `bandara_keberangkatan`, `bandara_kedatangan`, `tanggal_keberangkatan`, `tanggal_kedatangan`, `waktu_keberangkatan`, `waktu_kedatangan`, `harga`, `ketersediaan_kursi`, `jenis_pesawat`, `maskapai_penerbangan_fk`)
VALUES 
  (6, 'Garuda Indonesia', 'Soekarno-Hatta International Airport', 'Ngurah Rai International Airport', '2023-07-15', '2023-07-15', '08:00:00', '10:30:00', 1800000, 150, 'Boeing 777', 'GA'),
  (7, 'AirAsia', 'Juanda International Airport', 'Kuala Lumpur International Airport', '2023-07-16', '2023-07-16', '10:30:00', '13:00:00', 900000, 100, 'Airbus A320', 'AA'),
  (8, 'Lion Air', 'Ngurah Rai International Airport', 'Soekarno-Hatta International Airport', '2023-07-17', '2023-07-17', '13:30:00', '16:00:00', 1400000, 120, 'Boeing 737', 'JT'),
  (9, 'Citilink', 'Husein Sastranegara International Airport', 'Juanda International Airport', '2023-07-18', '2023-07-18', '15:00:00', '17:30:00', 700000, 80, 'Airbus A320', 'QG'),
  (10, 'Batik Air', 'Adisutjipto International Airport', 'Soekarno-Hatta International Airport', '2023-07-19', '2023-07-19', '18:00:00', '20:30:00', 950000, 100, 'Airbus A320', 'ID'),
  (11, 'Garuda Indonesia', 'Juanda International Airport', 'Ngurah Rai International Airport', '2023-07-20', '2023-07-20', '09:00:00', '11:30:00', 1700000, 150, 'Boeing 777', 'GA'),
  (12, 'AirAsia', 'Kuala Lumpur International Airport', 'Soekarno-Hatta International Airport', '2023-07-21', '2023-07-21', '11:30:00', '14:00:00', 850000, 100, 'Airbus A320', 'AA'),
  (13, 'Lion Air', 'Soekarno-Hatta International Airport', 'Juanda International Airport', '2023-07-22', '2023-07-22', '14:30:00', '17:00:00', 1300000, 120, 'Boeing 737', 'JT'),
  (14, 'Citilink', 'Juanda International Airport', 'Husein Sastranegara International Airport', '2023-07-23', '2023-07-23', '16:00:00', '18:30:00', 650000, 80, 'Airbus A320', 'QG'),
  (15, 'Batik Air', 'Soekarno-Hatta International Airport', 'Adisutjipto International Airport', '2023-07-24', '2023-07-24', '19:00:00', '21:30:00', 900000, 100, 'Airbus A320', 'ID');
```

# - Insert pada Tabel Maskapai Penerbangan
Berikut ini adalah syntax SQL untuk menginput data pada database tabel Maskapai Penerbangan
```mySQL
INSERT INTO `MaskapaiPenerbangan` (`id_maskapai`, `nama_maskapai`, `kode_maskapai`, `negara_asal`)
VALUES 
  (1, 'Garuda Indonesia', 'GA', 'Indonesia'),
  (2, 'AirAsia', 'AA', 'Malaysia'),
  (3, 'Lion Air', 'JT', 'Indonesia'),
  (4, 'Citilink', 'QG', 'Indonesia'),
  (5, 'Batik Air', 'ID', 'Indonesia'),
  (6, 'Singapore Airlines', 'SQ', 'Singapura'),
  (7, 'Emirates', 'EK', 'Uni Emirat Arab'),
  (8, 'Cathay Pacific', 'CX', 'Hong Kong'),
  (9, 'Qatar Airways', 'QR', 'Qatar'),
  (10, 'Thai Airways', 'TG', 'Thailand');
  ```

# - Insert pada Tabel Pemesanan
Berikut ini adalah syntax SQL untuk menginput data pada database tabel Maskapai Pemesanan
```mySQL
INSERT INTO `Pemesanan` (`id_pemesanan`, `id_pengguna`, `tanggal_pemesanan`, `total_harga`, `status`, `id_pengguna_fk`)
VALUES 
  (1, 1, '2023-07-01', 2500000, 'Lunas', 1),
  (2, 2, '2023-07-02', 1800000, 'Lunas', 2),
  (3, 1, '2023-07-03', 1200000, 'Lunas', 1),
  (4, 3, '2023-07-04', 1500000, 'Belum Lunas', 3),
  (5, 2, '2023-07-05', 2000000, 'Lunas', 2),
  (6, 3, '2023-07-06', 900000, 'Belum Lunas', 3),
  (7, 4, '2023-07-07', 1700000, 'Lunas', 4),
  (8, 4, '2023-07-08', 1300000, 'Belum Lunas', 4),
  (9, 1, '2023-07-09', 1400000, 'Lunas', 1),
  (10, 3, '2023-07-10', 1100000, 'Belum Lunas', 3);
```

# - Insert pada Tabel Pemesanan Hotel
Berikut ini adalah syntax SQL untuk menginput data pada database tabel Maskapai Pemesanan Hotel
```mySQL
INSERT INTO `PemesananHotel` (`id_pemesanan_hotel`, `id_pemesanan`, `id_hotel`, `tanggal_check_in`, `tanggal_check_out`, `jumlah_kamar`, `jumlah_tamu`, `id_pemesanan_fk`, `id_hotel_fk`)
VALUES 
  (1, 1, 1, '2023-07-02', '2023-07-05', 2, 4, 1, 1),
  (2, 2, 3, '2023-07-03', '2023-07-06', 1, 2, 2, 3),
  (3, 3, 2, '2023-07-04', '2023-07-07', 1, 1, 3, 2),
  (4, 4, 5, '2023-07-05', '2023-07-08', 3, 6, 4, 5),
  (5, 5, 4, '2023-07-06', '2023-07-09', 2, 3, 5, 4),
  (6, 6, 1, '2023-07-07', '2023-07-10', 1, 2, 6, 1),
  (7, 7, 2, '2023-07-08', '2023-07-11', 1, 1, 7, 2),
  (8, 8, 3, '2023-07-09', '2023-07-12', 1, 2, 8, 3),
  (9, 9, 4, '2023-07-10', '2023-07-13', 2, 4, 9, 4),
  (10, 10, 5, '2023-07-11', '2023-07-14', 1, 1, 10, 5);
```

# - Insert pada Tabel Pemesanan Penerbangan
Berikut ini adalah syntax SQL untuk menginput data pada database tabel Maskapai Pemesanan Penerbangan
```mySQL
INSERT INTO `PemesananPenerbangan` (`id_pemesanan_penerbangan`, `id_pemesanan`, `id_penerbangan`, `jumlah_penumpang`, `id_pemesanan_fk`, `id_penerbangan_fk`)
VALUES 
  (1, 1, 1, 2, 1, 1),
  (2, 2, 3, 1, 2, 3),
  (3, 3, 2, 1, 3, 2),
  (4, 4, 5, 3, 4, 5),
  (5, 5, 4, 2, 5, 4),
  (6, 6, 1, 1, 6, 1),
  (7, 7, 2, 1, 7, 2),
  (8, 8, 3, 1, 8, 3),
  (9, 9, 4, 2, 9, 4),
  (10, 10, 5, 1, 10, 5); 
```

# - Insert pada Tabel Pembayaran
Berikut ini adalah syntax SQL untuk menginput data pada database tabel Maskapai Pembayaran
```mySQL
INSERT INTO `Pembayaran` (`id_pembayaran`, `id_pemesanan`, `tanggal_pembayaran`, `jumlah`, `metode_pembayaran`, `status_pembayaran`, `id_pemesanan_fk`)
VALUES 
  (1, 1, '2023-07-02', 2500000, 'Transfer Bank', 'Lunas', 1),
  (2, 2, '2023-07-03', 1800000, 'Kartu Kredit', 'Lunas', 2),
  (3, 3, '2023-07-04', 1200000, 'Transfer Bank', 'Lunas', 3),
  (4, 4, '2023-07-05', 1500000, 'Kartu Kredit', 'Belum Lunas', 4),
  (5, 5, '2023-07-06', 2000000, 'Transfer Bank', 'Lunas', 5),
  (6, 6, '2023-07-07', 900000, 'Kartu Kredit', 'Belum Lunas', 6),
  (7, 7, '2023-07-08', 1700000, 'Transfer Bank', 'Lunas', 7),
  (8, 8, '2023-07-09', 1300000, 'Kartu Kredit', 'Belum Lunas', 8),
  (9, 9, '2023-07-10', 1400000, 'Transfer Bank', 'Lunas', 9),
  (10, 10, '2023-07-11', 1100000, 'Kartu Kredit', 'Belum Lunas', 10);
```
# - Insert pada Tabel Review
Berikut ini adalah syntax SQL untuk menginput data pada database tabel Maskapai Review
```mySQL
INSERT INTO `Review` (`id_review_hotel`, `id_review_penerbangan`, `id_pengguna`, `id_hotel`, `id_penerbangan`, `tanggal_review`, `rating`, `komentar`, `id_pengguna_fk`, `id_hotel_fk`, `id_penerbangan_fk`)
VALUES 
  (1, 1, 1, 1, 1, '2023-07-02', 4.5, 'Hotel yang bagus dengan pelayanan yang memuaskan', 1, 1, 1),
  (2, 2, 2, 3, 3, '2023-07-03', 4.0, 'Penerbangan nyaman dengan makanan yang lezat', 2, 3, 3),
  (3, 3, 1, 2, 2, '2023-07-04', 5.0, 'Hotel yang luar biasa dengan pemandangan yang indah', 1, 2, 2),
  (4, 4, 3, 5, 5, '2023-07-05', 4.2, 'Penerbangan yang baik dengan awak kabin yang ramah', 3, 5, 5),
  (5, 5, 2, 4, 4, '2023-07-06', 4.8, 'Hotel yang nyaman dengan fasilitas lengkap', 2, 4, 4),
  (6, 6, 3, 1, 1, '2023-07-07', 3.5, 'Penerbangan biasa saja dengan sedikit keterlambatan', 3, 1, 1),
  (7, 7, 4, 2, 2, '2023-07-08', 4.7, 'Hotel yang luar biasa dengan staf yang ramah', 4, 2, 2),
  (8, 8, 4, 3, 3, '2023-07-09', 4.0, 'Penerbangan yang nyaman dengan kursi yang lega', 4, 3, 3),
  (9, 9, 1, 4, 4, '2023-07-10', 4.6, 'Hotel yang mengesankan dengan lokasi strategis', 1, 4, 4),
  (10, 10, 3, 5, 5, '2023-07-11', 4.3, 'Penerbangan yang baik dengan pelayanan yang ramah', 3, 5, 5);
```

# - Read
Berikut ini adalah syntax untuk membaca data tabel
```mySQL
SELECT * FROM MaskapaiPenerbangan 
WHERE negara_asal = 'Indonesia';
```

# - Update
Berikut ini adalah syntax untuk merubah data tabel
```mySQL
UPDATE Pemesanan
SET status = 'Lunas'
WHERE id_pemesanan = 1;
```

# - Read
Berikut ini adalah syntax untuk menghapus data tabel
```mySQL
DELETE FROM Pemesanan
WHERE id_pemesanan = 1;
```

# Nomor 4
# DQL (Data Query Language)
Digunakan untuk melakukan query atau pengambilan data dari basis data tanpa melakukan perubahan pada data itu sendiri.

Berikut ini adalah analisis pertanyaan maupun pernyataan yang biasa muncul

| No. | Use Case                                   |
|-----|--------------------------------------------|
| 1   | Berikan nama hotel dan kota dari hotel dengan peringkat bintang diatas 4  ?                       |
| 2   | Jumlahkan total harga dari semua pemesanan yang memiliki status lunas!                               |
| 3   | Tampilkan daftar Penerbangan yang berangkat dari Jakarta ke Bali!                        |
| 4   | Tampilkan data Pembayaran beserta informasi pemesanannya!                            |
| 5   | Tampilkan semua hotel dan review hotelnya!                  |
| 6   | Urutkan daftar pengguna berdasarkan nama_depan secara ascending!                         |
| 7   | Mengurutkan daftar Pembayaran berdasarkan jumlah secara descending!                               |
| 8   | Berikan rekap jumlah pemesanan berdasarkan statusnya!                   |
| 9   | Tampilkan rata-rata peringkat berdasarkan nama hotel!                                |
| 10  | Menampilkan data pemesanan hotel beserta informasi hotel yang terkait, diurutkan berdasarkan tanggal_pemesanan secara terbalik                          |
| 11  | Menampilkan data pembayaran beserta informasi pemesanan yang terkait, diurutkan berdasarkan jumlah pembayaran secara terbalik             |
| 12  | Menampilkan total harga pemesanan berdasarkan tanggal pemesanan, diurutkan dari yang tertinggi ke terendah                            |



## 1. Berikan nama hotel dan kota dari hotel dengan peringkat bintang diatas 4  ?  
```mySQL
SELECT nama_hotel, kota FROM Hotel WHERE peringkat_bintang > 4;
```

## 2. Jumlahkan total harga dari semua pemesanan yang memiliki status lunas!
```mySQL
SELECT SUM(total_harga) AS total_harga_lunas FROM Pemesanan WHERE status = 'Lunas';
```

## 3. Tampilkan daftar Penerbangan yang berangkat dari Jakarta ke Bali!
```mySQL
SELECT * FROM Penerbangan WHERE bandara_keberangkatan = 'Jakarta' AND bandara_kedatangan = 'Bali';
```

## 4. Tampilkan data Pembayaran beserta informasi pemesanannya!
```mySQL
SELECT pb.id_pembayaran, pb.id_pemesanan, pb.tanggal_pembayaran, pb.jumlah, p.status
FROM Pembayaran pb
INNER JOIN Pemesanan p ON pb.id_pemesanan = p.id_pemesanan;
```

 ## 5. Tampilkan semua hotel dan review hotelnya!
```mySQL
SELECT h.nama_hotel, h.alamat, r.id_review_hotel, r.rating, r.komentar
FROM Hotel h
LEFT JOIN Review r ON h.id_hotel = r.id_hotel;
```

## 6. Urutkan daftar pengguna berdasarkan nama_depan secara ascending!
```mySQL
SELECT * FROM Pengguna ORDER BY nama_depan ASC;
```

## 7. Mengurutkan daftar Pembayaran berdasarkan jumlah secara descending!
```mySQL
SELECT * FROM Pembayaran ORDER BY jumlah DESC;
```

## 8. Berikan rekap jumlah pemesanan berdasarkan statusnya!
```mySQL
SELECT status, COUNT(*) AS jumlah_pemesanan
FROM Pemesanan
GROUP BY status;
```

## 9. Tampilkan rata-rata peringkat berdasarkan nama hotel!
```mySQL
SELECT nama_hotel, AVG(rata_rata_peringkat) AS rata_rata_peringkat_hotel
FROM Hotel
GROUP BY nama_hotel;
```

## 10. Menampilkan data pemesanan hotel beserta informasi hotel yang terkait, diurutkan berdasarkan tanggal_pemesanan secara terbalik
```mySQL
SELECT ph.id_pemesanan_hotel, ph.id_pemesanan, ph.id_hotel, h.nama_hotel, h.alamat
FROM PemesananHotel ph
INNER JOIN Hotel h ON ph.id_hotel = h.id_hotel
ORDER BY ph.id_pemesanan_hotel DESC
LIMIT 1000;
```

## 11. Menampilkan data pembayaran beserta informasi pemesanan yang terkait, diurutkan berdasarkan jumlah pembayaran secara terbalik
```mySQL
SELECT pb.id_pembayaran, pb.id_pemesanan, pb.tanggal_pembayaran, pb.jumlah, p.status
FROM Pembayaran pb
INNER JOIN Pemesanan p ON pb.id_pemesanan = p.id_pemesanan
ORDER BY pb.jumlah DESC;
```

## 12. Menampilkan total harga pemesanan berdasarkan tanggal pemesanan, diurutkan dari yang tertinggi ke terendah
```mySQL
SELECT tanggal_pemesanan, SUM(total_harga) AS total_harga_pemesanan
FROM Pemesanan
GROUP BY tanggal_pemesanan
ORDER BY total_harga_pemesanan DESC;
```

# Nomor 5
## Berikut ini adalah video demo penjelasan peroject yang saya buat
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/DC1934a0S84" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->
