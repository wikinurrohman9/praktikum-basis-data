# Nomor 1
## Menampilkan Use Case Tabel Untuk Produk Digitalnya

### - Tabel User
| No | Use Case User|
|---|------------|
| 1 | Seorang pengguna dapat melakukan pencarian hotel berdasarkan kota atau peringkat bintang. |
| 2 | Seorang pengguna dapat melihat detail hotel, termasuk alamat, fasilitas, dan rata-rata peringkat.|
| 3 | Seorang pengguna dapat melakukan pemesanan hotel dengan memilih tanggal check-in, tanggal check-out, jumlah kamar, dan jumlah tamu.|
| 4 | Seorang pengguna dapat melihat status pemesanan dan melakukan pembayaran untuk pemesanan yang telah dilakukan.|
| 5 | Seorang pengguna dapat memberikan ulasan dan rating untuk hotel yang telah dikunjungi.|

### - Tabel Manajemen
| No | Use Case Manajemen|
|---|------------|
| 1 | Manajemen hotel dapat melihat daftar pemesanan hotel yang telah dibuat.|
| 2 | Manajemen hotel dapat mengelola ketersediaan kamar hotel berdasarkan pemesanan yang telah dilakukan.|
| 3 | Manajemen hotel dapat melihat rata-rata peringkat hotel dan melakukan perbaikan jika ada masalah yang diidentifikasi dalam ulasan pengguna.|
| 4 | Manajemen hotel dapat mengelola fasilitas dan deskripsi hotel yang ditampilkan kepada pengguna.|

### - Tabel Direksi
| No | Use Case Direksi|
|---|------------|
| 1 | Direksi dapat melihat laporan keuangan berdasarkan total pendapatan dari pemesanan hotel.|
| 2 | Direksi dapat melihat analisis peringkat dan ulasan hotel untuk mengevaluasi kepuasan pengguna.|
| 3 | Direksi dapat melihat perkembangan jumlah pengguna baru dan jumlah pemesanan dalam periode tertentu.|
| 4 | Direksi dapat melakukan analisis data untuk mengidentifikasi tren dan kesempatan baru dalam industri perhotelan.|

---
# Nomor 2
## Mampu mendemonstrasikan web service (CRUD) dari produk digitalnya
Screenshot koneksi ke database dari bahasa pemrograman yang dipilih
Bentuk web servicenya:

RESTful API
GraphQL

Interface untuk eksplorasi web servicenya (Hoppscotch / Postman / Swagger)

---
# Nomor 3
## Mampu mendemonstrasikan minimal 3 visualisasi data untuk business intelligence produk digitalnya
Dalam visualisasi ini saya menggunakan tools `metabase` untuk memvisualisasikan database traveloka saya. Dibawah ini terdapat beberapa visualisasi dengan bermacam grafik

## - Menampilkan Data Rata-rata Jumlah Pembayaran *(day-of-month)* dengan Visualisasi Diagram Garis

Dibuatnya diagram ini bertujuan agar Manajemen maupun Direksi dapat mengetahui perkembangan pembayaran setiap harinya.

![Rata-rata_pembayaran](/uploads/ce0c51def63012d9cf3f485772f56031/Rata-rata_pembayaran.PNG)

## - Menampilkan Data Jenis Kelamin Pengguna dengan Visualisasi Pie

Dengan adanya visualisasi ini Manajemen maupun Direksi dapat mengetahui seberapa banyak jumlah penumpang berdasarkan jenis kelaminya.

![Jenis_Kelamin](/uploads/1d32eedcb96ac6d3b202f748234c926b/Jenis_Kelamin.PNG)

## - Menampilkan Data Status Pembayaran dengan Visualisasi Diagram Batang

Manajemen maupun Direksi dapat mengetahui jumlah yang belum melakukan pelunasan dan yang sudah melakukan pelunasan, juga dapat mengetahui rata-rata di setiap pelunasan dan belum pelunasan. 

![Status_Pembayaran](/uploads/be6cea1c420a781e5d5acd1525948757/Status_Pembayaran.PNG)

---
# Nomor 4
## Mampu mendemonstrasikan penggunaan minimal 3 built-in function dengan ketentuan :

### 1. Concat
Query ini menggabungkan kolom nama_depan dan nama_belakang dari tabel Pengguna menjadi satu kolom baru bernama nama_lengkap. Fungsi CONCAT digunakan untuk menggabungkan beberapa string menjadi satu string.

```sql
SELECT CONCAT(nama_depan, ' ', nama_belakang) AS nama_lengkap FROM Pengguna;
```

![Concat](/uploads/f2081f575b0b43f8b711e6cf838b53a1/Concat.PNG)

### 2. Count
Query ini menghitung jumlah total pemesanan yang ada. Fungsi COUNT digunakan untuk menghitung jumlah baris yang sesuai dengan kondisi yang diberikan.

```sql
SELECT COUNT(*) AS total_pemesanan FROM Pemesanan;

```

![Count](/uploads/d4ce47243f13c40e4b6496fa3e0a4057/Count.PNG)

### 3. Avg
Query ini menghitung rata-rata peringkat (rating) dari semua ulasan (review) hotel. Fungsi AVG digunakan untuk menghitung rata-rata nilai dalam sebuah kolom.

```sql
SELECT AVG(rating) AS rata_rata_rating FROM Review WHERE id_review_hotel IS NOT NULL;
```

![avg](/uploads/c28c0f5ecfac8d3c81d029b0e9906fe8/avg.PNG)


### 4. UPPER dan LOWER
Query ini mengambil semua pengguna dengan nama pengguna yang diubah menjadi huruf kapital dan huruf kecil. Fungsi UPPER digunakan untuk mengubah string menjadi huruf kapital, sedangkan LOWER digunakan untuk mengubah string menjadi huruf kecil.

```sql
SELECT UPPER(nama_pengguna) AS nama_pengguna_kapital, LOWER(nama_pengguna) AS nama_pengguna_kecil FROM Pengguna;
```

![Uper_Lowper](/uploads/8f69bb4c03ff0ea0201ce75a99f22ee6/Uper_Lowper.PNG)

### 5. Regex
Query ini mengambil semua pengguna dengan email yang sesuai dengan pola tertentu. Fungsi REGEXP digunakan untuk mencocokkan pola *(pattern matching)* menggunakan ekspresi reguler.

```sql
SELECT * FROM Pengguna WHERE email REGEXP '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$';
```

![Regex](/uploads/cba2a421dd65b74a571d58c544241de1/Regex.PNG)

### 6. Substring
Query ini mengambil sebagian nama depan dari pengguna. Fungsi SUBSTRING digunakan untuk mengambil sebagian dari sebuah string. Pada contoh di atas, fungsi ini mengambil tiga karakter pertama dari kolom nama_depan.

```sql
SELECT SUBSTRING(nama_depan, 1, 3) AS nama_depan_pendek FROM Pengguna;
```

![Substring](/uploads/e4abc245b675a8865e619c41a02395e7/Substring.PNG)

---
# Nomor 5
## Mampu mendemonstrasikan dan menjelaskan penggunaan Subquery pada produk digitalnya
Subquery merupakan sebuah query yang diletakkan di dalam query utama. Hasil dari subquery dapat digunakan dalam kondisi, SELECT, INSERT, UPDATE, atau DELETE statement pada query utama. Subquery sering digunakan untuk melakukan penghitungan atau filtering data yang kompleks.

Manajemen dan Direksi ingin mencari semua pemesanan hotel yang memiliki jumlah kamar lebih dari rata-rata jumlah kamar dari semua pemesanan hotel oleh pengguna dengan jenis kelamin tertentu. Berikut ini adalah syntax sqlnya:
```sql
SELECT id_pemesanan_hotel, id_pemesanan, id_hotel, tanggal_check_in, tanggal_check_out, jumlah_kamar, jumlah_tamu
FROM PemesananHotel
WHERE jumlah_kamar > (
  SELECT AVG(jumlah_kamar)
  FROM PemesananHotel
  WHERE id_pemesanan IN (
    SELECT id_pemesanan
    FROM Pemesanan
    WHERE id_pengguna IN (
      SELECT id_pengguna
      FROM Pengguna
      WHERE jenis_kelamin = 'Laki-laki'
    )
  )
);
```


### - Subquery pertama:
```sql
SELECT id_pengguna
FROM Pengguna
WHERE jenis_kelamin = 'Laki-laki'
```
Subquery ini mengambil semua id_pengguna dari tabel Pengguna yang memiliki jenis_kelamin 'Laki-laki'.

### - Subquery kedua:
```sql
SELECT id_pemesanan
FROM Pemesanan
WHERE id_pengguna IN (Subquery Pertama)
```
Subquery ini mengambil semua id_pemesanan dari tabel Pemesanan yang memiliki id_pengguna yang ada dalam hasil Subquery Pertama.

### - Subquery ketiga:
```sql
SELECT AVG(jumlah_kamar)
FROM PemesananHotel
WHERE id_pemesanan IN (Subquery Kedua)
```
Subquery ini menghitung rata-rata jumlah_kamar dari semua pemesanan hotel yang memiliki id_pemesanan yang ada dalam hasil Subquery Kedua.

### - Subquery utama:
```sql
SELECT id_pemesanan_hotel, id_pemesanan, id_hotel, tanggal_check_in, tanggal_check_out, jumlah_kamar, jumlah_tamu
FROM PemesananHotel
WHERE jumlah_kamar > (Subquery Ketiga)
```
Subquery ini mengambil semua kolom dari tabel PemesananHotel yang memiliki jumlah_kamar lebih dari hasil Subquery Ketiga.


---
# Nomor 6
## Mampu mendemonstrasikan dan menjelaskan penggunaan Transaction pada produk digitalnya
`TRANSACTION` adalah sebuah unit kerja logis yang terdiri dari satu atau beberapa perintah SQL yang dieksekusi sebagai satu kesatuan yang tidak terbagi. Transaksi digunakan untuk memastikan bahwa serangkaian perubahan data dalam database dilakukan dengan konsistensi, dan entah semua perubahan tersebut berhasil dilakukan atau sebaliknya tidak ada perubahan yang disimpan.

```sql
START TRANSACTION;

-- Query untuk melakukan pembayaran pemesanan hotel
UPDATE PemesananHotel
SET status_pembayaran = 'Berhasil'
WHERE id_pemesanan = 1;

-- Query untuk melakukan pembayaran pemesanan penerbangan
UPDATE PemesananPenerbangan
SET status_pembayaran = 'Berhasil'
WHERE id_pemesanan = 1;

-- Cek apakah kedua pembayaran berhasil
IF (SELECT COUNT(*) FROM PemesananHotel WHERE id_pemesanan = 1 AND status_pembayaran = 'Berhasil') = 1 AND
   (SELECT COUNT(*) FROM PemesananPenerbangan WHERE id_pemesanan = 1 AND status_pembayaran = 'Berhasil') = 1 THEN
    COMMIT;
ELSE
    ROLLBACK;
END IF;

COMMIT;

```

---
# Nomor 7
## Mampu mendemonstrasikan dan menjelaskan penggunaan Procedure / Function dan Trigger pada produk digitalnya


### - Procedure
Procedure adalah kumpulan pernyataan SQL yang diberi nama dan disimpan dalam database. Mereka digunakan untuk melakukan tugas-tugas yang kompleks dan sering digunakan, seperti mengambil, memperbarui, atau menghapus data.

Misalkan kita ingin membuat sebuah procedure yang menghitung jumlah pemesanan hotel berdasarkan id pengguna.

```sql
DELIMITER //

CREATE PROCEDURE HitungJumlahPemesananHotel(IN idPengguna INT, OUT jumlahPemesanan INT)
BEGIN
  SELECT COUNT(*) INTO jumlahPemesanan
  FROM PemesananHotel
  WHERE id_pengguna = idPengguna;
END //

DELIMITER ;
```

Misalkan kita ingin memanggil procedure HitungJumlahPemesananHotel untuk menghitung jumlah pemesanan hotel berdasarkan id pengguna tertentu. 
```sql
SET @jumlahPemesanan := NULL;

CALL HitungJumlahPemesananHotel(1, @jumlahPemesanan);

SELECT IFNULL(@jumlahPemesanan, 0) AS jumlah_pemesanan;
```

### Function
Function, seperti halnya procedure, adalah kumpulan pernyataan SQL yang diberi nama dan disimpan dalam database. Namun, perbedaan utamanya adalah bahwa function selalu mengembalikan nilai, sedangkan procedure tidak perlu mengembalikan nilai. Function dapat digunakan dalam pernyataan SQL sebagai ekspresi, dan mereka juga dapat dipanggil dari procedure atau trigger.

Misalkan kita ingin membuat sebuah fungsi yang menghitung total harga pemesanan hotel berdasarkan id pemesanan.

```sql
CREATE FUNCTION HitungTotalHargaPemesananHotel(idPemesanan INT) RETURNS DECIMAL
DETERMINISTIC
BEGIN
  DECLARE totalHarga DECIMAL;
  SELECT SUM(total_harga) INTO totalHarga
  FROM PemesananHotel
  WHERE id_pemesanan = idPemesanan;
  RETURN totalHarga;
END;
```

### - Trigger
Trigger adalah blok kode yang dieksekusi secara otomatis ketika suatu peristiwa terjadi di database, seperti penghapusan, pembaruan, atau penambahan data pada tabel tertentu. Trigger berguna untuk menerapkan logika bisnis yang kompleks yang memerlukan tindakan otomatis setelah perubahan data terjadi.

Misalkan kita ingin membuat sebuah trigger yang akan memperbarui rata-rata peringkat (rating) pada tabel Hotel setiap kali ada ulasan baru (review) yang ditambahkan ke tabel Review. Trigger ini akan menghitung ulang rata-rata peringkat berdasarkan ulasan terbaru.

```sql
CREATE TRIGGER update_rata_rata_peringkat AFTER INSERT ON Review
FOR EACH ROW
BEGIN
  DECLARE total_rating DECIMAL;
  DECLARE jumlah_review INT;

  -- Mengambil total rating dan jumlah review untuk hotel terkait
  SELECT SUM(rating), COUNT(*) INTO total_rating, jumlah_review
  FROM Review
  WHERE id_hotel = NEW.id_hotel;

  -- Memperbarui rata-rata peringkat pada tabel Hotel
  UPDATE Hotel
  SET rata_rata_peringkat = total_rating / jumlah_review
  WHERE id_hotel = NEW.id_hotel;
END;
```

---
# Nomor 8
## Mampu mendemonstrasikan Data Control Language (DCL) pada produk digitalnya
DCL (Data Control Language) adalah kumpulan perintah yang digunakan untuk mengendalikan dan mengelola hak akses dan izin dalam sistem manajemen database. Ini memungkinkan administrator untuk memberikan atau mencabut izin kepada pengguna atau peran untuk melakukan tindakan tertentu pada objek database.

### - Memberikan Izin:
Untuk memberikan izin kepada pengguna atau peran, Anda dapat menggunakan pernyataan `GRANT`. Misalnya, mari berikan hak SELECT pada tabel Hotel kepada pengguna bernama 'traveloka_user':

```sql
GRANT SELECT ON Hotel TO traveloka_user;
```

### - Mencabut Izin:
Untuk mencabut izin dari pengguna atau peran, Anda dapat menggunakan pernyataan `REVOKE`. Misalnya, mari cabut hak INSERT pada tabel Penerbangan dari peran bernama 'traveloka_agent':

```sql
REVOKE INSERT ON Penerbangan FROM traveloka_agent;
```

### - Membuat Peran:
Peran dapat dibuat untuk mengelompokkan pengguna dengan persyaratan akses yang serupa. Peran dapat menyederhanakan pengelolaan izin dengan memberikan atau mencabut izin untuk sekelompok pengguna. Misalnya, mari buat peran bernama 'traveloka_manager':

```sql
CREATE ROLE traveloka_manager;
```

### - Memberikan Izin kepada Peran:
Izin dapat diberikan kepada peran daripada pengguna individual. Dengan cara ini, setiap pengguna yang ditugaskan ke peran tersebut akan mewarisi izin yang diberikan. Misalnya, mari berikan hak `UPDATE` pada tabel Pengguna kepada peran 'traveloka_manager':

```sql
GRANT UPDATE ON Pengguna TO traveloka_manager;
```

### - Menugaskan Peran kepada Pengguna:
Pengguna dapat ditugaskan ke satu atau lebih peran untuk mewarisi izin yang terkait dengan peran-peran tersebut. Misalnya, mari tugaskan peran 'traveloka_manager' kepada pengguna bernama 'traveloka_admin':

```sql
GRANT traveloka_manager TO traveloka_admin;
```

### - Mencabut Peran dari Pengguna:
Peran dapat dicabut dari pengguna untuk menghapuskan izin yang diwarisi. Misalnya, mari cabut peran 'traveloka_manager' dari pengguna 'traveloka_admin':

```sql
REVOKE traveloka_manager FROM traveloka_admin;
```

---
# Nomor 9
## Mampu mendemonstrasikan dan menjelaskan constraint yang digunakan pada produk digitalnya:

### - Foreign Key
`FOREIGN KEY` adalah sebuah constraint yang digunakan untuk membangun hubungan antara dua tabel dalam sebuah database relasional.
`Constraint` ini digunakan untuk memastikan integritas referensial antara kolom dalam satu tabel (yang disebut sebagai tabel asal) dengan kolom dalam tabel lain (yang disebut sebagai tabel tujuan) dengan menggunakan nilai-nilai kunci.
`FOREIGN KEY` memungkinkan Anda untuk membuat hubungan antara dua tabel yang memiliki kolom yang saling berhubungan. Hal ini memungkinkan penggunaan operasi join untuk mengambil data yang berkaitan dari dua tabel yang terhubung.

```sql
CREATE TABLE `Penerbangan` (
  `id_penerbangan` int PRIMARY KEY,
  `maskapai_penerbangan_fk` int,
  `bandara_keberangkatan` varchar(255),
  `bandara_kedatangan` varchar(255),
  `tanggal_keberangkatan` date,
  `tanggal_kedatangan` date,
  `waktu_keberangkatan` time,
  `waktu_kedatangan` time,
  `harga` decimal,
  `ketersediaan_kursi` int,
  `jenis_pesawat` varchar(255),
  FOREIGN KEY (`maskapai_penerbangan_fk`) REFERENCES `MaskapaiPenerbangan` (`id_maskapai`)
);

CREATE TABLE `Pemesanan` (
  `id_pemesanan` int PRIMARY KEY,
  `id_pengguna_fk` int,
  `tanggal_pemesanan` date,
  `total_harga` decimal,
  `status` varchar(255),
  FOREIGN KEY (`id_pengguna_fk`) REFERENCES `Pengguna` (`id_pengguna`)
);

CREATE TABLE `PemesananHotel` (
  `id_pemesanan_hotel` int PRIMARY KEY,
  `id_pemesanan_fk` int,
  `id_hotel_fk` int,
  `tanggal_check_in` date,
  `tanggal_check_out` date,
  `jumlah_kamar` int,
  `jumlah_tamu` int,
  FOREIGN KEY (`id_pemesanan_fk`) REFERENCES `Pemesanan` (`id_pemesanan`),
  FOREIGN KEY (`id_hotel_fk`) REFERENCES `Hotel` (`id_hotel`)
);

CREATE TABLE `PemesananPenerbangan` (
  `id_pemesanan_penerbangan` int PRIMARY KEY,
  `id_pemesanan_fk` int,
  `id_penerbangan_fk` int,
  `jumlah_penumpang` int,
  FOREIGN KEY (`id_pemesanan_fk`) REFERENCES `Pemesanan` (`id_pemesanan`),
  FOREIGN KEY (`id_penerbangan_fk`) REFERENCES `Penerbangan` (`id_penerbangan`)
);

CREATE TABLE `Pembayaran` (
  `id_pembayaran` int PRIMARY KEY,
  `id_pemesanan_fk` int,
  `tanggal_pembayaran` date,
  `jumlah` decimal,
  `metode_pembayaran` varchar(255),
  `status_pembayaran` varchar(255),
  FOREIGN KEY (`id_pemesanan_fk`) REFERENCES `Pemesanan` (`id_pemesanan`)
);

CREATE TABLE `Review` (
  `id_review_hotel` int,
  `id_review_penerbangan` int,
  `id_pengguna_fk` int,
  `id_hotel_fk` int,
  `id_penerbangan_fk` int,
  `tanggal_review` date,
  `rating` decimal,
  `komentar` varchar(255),
  PRIMARY KEY (`id_review_hotel`, `id_review_penerbangan`),
  FOREIGN KEY (`id_pengguna_fk`) REFERENCES `Pengguna` (`id_pengguna`),
  FOREIGN KEY (`id_hotel_fk`) REFERENCES `Hotel` (`id_hotel`),
  FOREIGN KEY (`id_penerbangan_fk`) REFERENCES `Penerbangan` (`id_penerbangan`)
);
```

### - Unique Key
`UNIQUE KEY` adalah sebuah constraint yang digunakan untuk memastikan bahwa nilai-nilai dalam kolom atau beberapa kolom pada sebuah tabel adalah unik dan tidak boleh ada duplikat.
`Constraint` ini memastikan bahwa tidak ada dua baris dalam tabel yang memiliki nilai yang sama dalam kolom yang ditetapkan sebagai UNIQUE KEY.
`UNIQUE KEY` dapat diterapkan pada satu atau beberapa kolom dalam sebuah tabel.
Dalam beberapa sistem manajemen basis data, UNIQUE KEY juga berperan sebagai indeks, yang dapat meningkatkan kinerja query pada kolom tersebut.

```sql
CREATE TABLE Review (
id_review_hotel int,
id_review_penerbangan int,
id_pengguna int,
id_hotel int,
id_penerbangan int,
tanggal_review date,
rating decimal,
komentar varchar(255),
id_pengguna_fk int,
id_hotel_fk int,
id_penerbangan_fk int,
PRIMARY KEY (id_review_hotel, id_review_penerbangan),
CONSTRAINT uniq_id_pengguna UNIQUE (id_pengguna),
CONSTRAINT uniq_id_hotel UNIQUE (id_hotel),
CONSTRAINT uniq_id_penerbangan UNIQUE (id_penerbangan)
);
```

---
# Nomor 10
## Mendemonstrasikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube




---
# Nomor 11
## Mendemonstrasikan UI untuk CRUDnya


